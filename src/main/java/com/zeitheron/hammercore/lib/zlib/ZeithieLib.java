package com.zeitheron.hammercore.lib.zlib;

public class ZeithieLib
{
	public static final String VERSION = "1.6.6";
	
	public static String getVersion()
	{
		return VERSION;
	}
}